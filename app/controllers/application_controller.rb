class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  before_filter :set_locale

  def log_in user=nil
    session[:user_id] = user.id if user
  end

  def current_user
    @current_user ||= User.find(session[:user_id]) if session[:user_id]
  end

  def set_locale
    I18n.locale = session[:locale] if session[:locale]
  end

  helper_method :current_user

  def require_login
    unless current_user
      flash[:danger] = "Necesitas logearte"
      redirect_to root_path
    end
  end

  def confirm_login
    if current_user
      redirect_to root_path
    end
  end


  def admin?
    if current_user.admin?
      true
    else
      flash[:danger] = "No puedes hacer eso"
      redirect_to root_path
    end
  end

  def jsonToActiveRecord array

    finalItems = []

    array.each do |item|

      if item["type"] == "disc"
        item = Disc.find item["id"]
      elsif item["type"] == "track"
        item = Track.find item["id"]
      end

      finalItems.push item
    end

    finalItems

  end

  def totalPrice array
    price = 0
    array.each do |item|
      price += item.price
    end

    price
  end

  helper_method :admin?end
