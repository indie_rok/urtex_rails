class ChangeAttributesForDiscTranslation < ActiveRecord::Migration
  def change
    rename_column :discs, :title, :title_es
    rename_column :discs, :description, :description_es
  end
end
