class UsersController < ApplicationController

  before_action :confirm_login

  def new
    @user = User.new
  end

  def create

    @user = User.new(user_params)

    if @user.save
      log_in @user
      flash[:success] = t('flash_login_exito')
      redirect_to root_path
    else
      flash[:danger] = @user.errors
      redirect_to new_user_path
    end

  end

  private
  def user_params
    params.require(:user).permit(:email,:password,:password_confirmation)
  end
end
