class Admin::Featured < ActiveRecord::Base
  translates :title

  has_one :attachment, as: :attachmentable
end
