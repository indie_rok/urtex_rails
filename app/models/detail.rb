class Detail < ActiveRecord::Base
  belongs_to :order
  belongs_to :detailmentable, polymorphic: true
end
