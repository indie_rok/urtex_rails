require "rails_helper"

RSpec.describe Admin::InterpretersController, type: :routing do
  describe "routing" do

    it "routes to #index" do
      expect(:get => "/admin/interpreters").to route_to("admin/interpreters#index")
    end

    it "routes to #new" do
      expect(:get => "/admin/interpreters/new").to route_to("admin/interpreters#new")
    end

    it "routes to #show" do
      expect(:get => "/admin/interpreters/1").to route_to("admin/interpreters#show", :id => "1")
    end

    it "routes to #edit" do
      expect(:get => "/admin/interpreters/1/edit").to route_to("admin/interpreters#edit", :id => "1")
    end

    it "routes to #create" do
      expect(:post => "/admin/interpreters").to route_to("admin/interpreters#create")
    end

    it "routes to #update via PUT" do
      expect(:put => "/admin/interpreters/1").to route_to("admin/interpreters#update", :id => "1")
    end

    it "routes to #update via PATCH" do
      expect(:patch => "/admin/interpreters/1").to route_to("admin/interpreters#update", :id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/admin/interpreters/1").to route_to("admin/interpreters#destroy", :id => "1")
    end

  end
end
