require 'rails_helper'

RSpec.describe "admin/interpreters/show", type: :view do
  before(:each) do
    @admin_interpreter = assign(:admin_interpreter, Admin::Interpreter.create!(
      :name => "Name"
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(/Name/)
  end
end
