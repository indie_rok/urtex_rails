class User < ActiveRecord::Base
  has_secure_password
  validates_presence_of :email, case_sensitive: false
  validates_length_of :password, minimum: 5, on: :create
  validates_uniqueness_of :email
  validates_format_of :email, :with => /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\z/i
  validates_confirmation_of :email

  has_many :payments
  has_many :orders

end
