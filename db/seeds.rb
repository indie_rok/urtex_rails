# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

authors = Author.create([
    {name:'Emmanuel Orozco'},
    {name: 'Juan hernandez'},
    {name: 'Joaquin gonzalez'},
])

genres = Genre.create([
    {name: 'Clasical 50'},
    {name: 'New Age'},
    {name: 'Pop Clasical'},
    {name: 'Collection Thomas', collection: true}
])

disc1 = Disc.create(
    {title: 'Rock and roll',
      description: 'This is a coool disc',
      price: 35.56,
      discUrl:'https://s3-us-west-2.amazonaws.com/urtex-seeds/Archive.zip'
    })


genre_covers = Attachment.create(
    [{url: 'https://s3-us-west-2.amazonaws.com/urtex-seeds/11.jpeg', amazonId:'234of',attachmentable_type:'Genre', attachmentable_id: genres.first.id},
      {url: 'https://s3-us-west-2.amazonaws.com/urtex-seeds/22.jpeg', amazonId:'asds20rigm',attachmentable_type:'Genre', attachmentable_id: genres.second.id}
  ])

author_covers = Attachment.create([
  {url: 'https://s3-us-west-2.amazonaws.com/urtex-seeds/33.jpeg', amazonId:'asdfdg45',attachmentable_type:'Author', attachmentable_id: authors.last.id},

  ])


disc1.update_attributes({genre: Genre.last})


disc2 = Disc.create(
    {title: 'Emmanuel Star',
      description: nil,
      price: '0.99',
      discUrl:'https://s3-us-west-2.amazonaws.com/urtex-seeds/3m_mailing_6.zip'

    })

    disc2.update_attributes({genre: Genre.last})

tracks = Track.create [
    {title:'Volans',url: 'https://s3-us-west-2.amazonaws.com/urtex-seeds/03+Baby%27s+On+Fire.mp3', amazonId:'aaabbb',disc_id: disc2.id,sampleUrl: 'https://s3-us-west-2.amazonaws.com/urtex-seeds/03+Shooting+Stars.mp3',price:0.99},
    {title:'Babys on Fire',url: 'https://s3-us-west-2.amazonaws.com/urtex-seeds/03+Baby%27s+On+Fire.mp3', amazonId:'cccdddd',disc_id: disc2.id,price:9.33},
    {title: 'Golden Path', url:'https://s3-us-west-2.amazonaws.com/urtex-seeds/08+Escape+Velocity_The+Golden+Path.mp3', sampleUrl:'https://s3-us-west-2.amazonaws.com/urtex-seeds/Arcade+Fire+-+Afterlife.mp3',amazonId:'asdasd',disc_id:disc1.id,price:12.55},
    {title:'Supersymetry', url: 'https://s3-us-west-2.amazonaws.com/urtex-seeds/Arcade+Fire+-+Supersymmetry.mp3', amazonId:'asdasdasdc',disc_id: disc1.id,price:9.22}
]


interpreters = Interpreter.create([{name:'Arcade Fire'},{name:'Bal bl car'}, {name:'Twitter'}])

tracks.first.interpreter_ids=[interpreters.first.id,interpreters.last.id]

tracks.second.interpreter_ids=[interpreters.second.id,interpreters.third.id]

tracks.fourth.interpreter_ids=[interpreters.first.id]

covers = Attachment.create [
    {url: 'https://s3-us-west-2.amazonaws.com/urtex-seeds/11.jpeg', amazonId:'coasd1',attachmentable_type:'Disc', attachmentable_id: disc1.id},
    {url: 'https://s3-us-west-2.amazonaws.com/urtex-seeds/22.jpeg', amazonId:'dfgdfg',attachmentable_type:'Disc', attachmentable_id: disc1.id},
    {url: 'https://s3-us-west-2.amazonaws.com/urtex-seeds/33.jpeg', amazonId:'dfg234dfg',attachmentable_type:'Disc', attachmentable_id: disc2.id},
]

disc1.track_ids = [tracks.third.id,tracks.fourth.id]
disc2.track_ids = [tracks.first.id,tracks.second.id]


user1 = User.create(email: 'email@email.com',password:'123123')

user2 = User.create(email:'new@email.com', password:'123123')

user3 = User.create(email:'cool@cool.com',password:'123123', admin:true)

orders = Order.create([
    {totalPrice:19,user_id:user1.id},
    {totalPrice:10.44,user_id:user1.id},
    {totalPrice:15.22,user_id:user2.id},
    {totalPrice:19.24,user_id:user2.id}
  ])

orderDetails = Detail.create([
    {detailmentable_id:4,detailmentable_type:'Track',order_id: orders.first},
    {detailmentable_id:2,detailmentable_type:'Track', order_id: orders.first},
    {detailmentable_id:2,detailmentable_type:'Disc',order_id: orders.second},
    {detailmentable_id:1,detailmentable_type:'Disc', order_id: orders.second},
    {detailmentable_id:2,detailmentable_type:'Track', order_id: orders.third},
    {detailmentable_id:2,detailmentable_type:'Disc', order_id: orders.third},
    {detailmentable_id:1,detailmentable_type:'Disc', order_id: orders.fourth},
    {detailmentable_id:1,detailmentable_type:'Track', order_id: orders.fourth},
  ])

payments = Payment.create([
    {amount:orders.first.totalPrice,user_id: user1.id,status:'Payed',code:'asdsd13',order_id:orders.first.id},
    {amount:orders.second.totalPrice,user_id: user1.id,status:'Payed',code:'asdsd12df3',order_id:orders.second.id},
    {amount:orders.third.totalPrice,user_id: user2.id,status:'Payed',code:'asdasd234',order_id:orders.third.id},
    {amount:orders.fourth.totalPrice,user_id: user2.id,status:'Payed',code:'wu492f',order_id:orders.fourth.id},

  ])

featureds = Admin::Featured.create([
    {title: 'Test title', url: 'discs/2'},
    {title:'this Is a really long title just beacuse I can', url:'discs/1'}
  ]
)

featureds.first.create_attachment(url:'https://s3-us-west-2.amazonaws.com/urtex-seeds/f1.jpeg',amazonId:'1d32722071f604f6dcf29b4195e86e3e')
featureds.last.create_attachment(url:'https://s3-us-west-2.amazonaws.com/urtex-seeds/f2.jpeg',amazonId:'46ec5a3cf3eb2fa2b4f5e178a458484b')
