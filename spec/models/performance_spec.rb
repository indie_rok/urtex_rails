require 'rails_helper'

RSpec.describe Performance, type: :model do
  it{ should belong_to :track }
  it{ should belong_to :interpreter }
end
