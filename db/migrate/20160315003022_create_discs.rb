class CreateDiscs < ActiveRecord::Migration
  def change
    create_table :discs do |t|
      t.string :title, null: false, uniqueness: true
      t.text :description
      t.decimal :price, default: 120
      t.belongs_to :genre
      t.belongs_to :author
      t.timestamps null: false
    end
  end
end
