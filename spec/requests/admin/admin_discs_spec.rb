require 'rails_helper'
require 'support/macros'

RSpec.describe "Admin::Discs", type: :request do
  describe "GET /admin_discs" do

    it "works when admin logs in" do
      admin = Fabricate(:user,admin: true)
      #require 'pry'; binding.pry
      rest_log_in admin
      get admin_discs_path
      expect(response).to have_http_status(200)
    end

    it 'does not work when user logs in' do
      user = Fabricate(:user)
      rest_log_in user
      get admin_discs_path
      expect(response).to have_http_status 302

    end

    it 'does not work when no log in' do
      get admin_discs_path
      expect(response).to have_http_status 302
    end

  end

end
