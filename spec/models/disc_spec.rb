require 'rails_helper'

RSpec.describe Disc, type: :model do
  it{ should have_many(:attachments) }
  it{ should have_many(:tracks) }
  it{ should belong_to :genre }
  it{ should belong_to :author }
end
