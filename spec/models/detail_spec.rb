require 'rails_helper'

RSpec.describe Detail, type: :model do
  it { should belong_to(:detailmentable) }
  it{ should belong_to :order }
end
