class Track < ActiveRecord::Base

  validates :url, presence: true
  validates :amazonId, presence: true, uniqueness: true
  validates :price, numericality: true


  belongs_to :disc
  has_many :performances, dependent: :destroy
  has_many :interpreters, :through => :performances
end