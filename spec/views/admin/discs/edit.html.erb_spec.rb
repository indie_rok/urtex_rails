require 'rails_helper'

RSpec.describe "admin/discs/edit", type: :view do
  before(:each) do
    @admin_disc = assign(:admin_disc, Disc.create!(title:'test'))
  end

  it "renders the edit admin_disc form" do
    render

    assert_select "form[action=?][method=?]", admin_disc_path(@admin_disc), "post" do
    end
  end
end
