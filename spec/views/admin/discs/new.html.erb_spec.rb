require 'rails_helper'

RSpec.describe "admin/discs/new", type: :view do
  before(:each) do
    assign(:admin_disc, Disc.new())
  end

  it "renders new admin_disc form" do
    render

    assert_select "form[action=?][method=?]", admin_discs_path, "post" do
    end
  end
end
