class AddCollectionToGenres < ActiveRecord::Migration
  def change
    add_column :genres, :collection, :boolean, default: false
  end
end
