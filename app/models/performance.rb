class Performance < ActiveRecord::Base
  belongs_to :track
  belongs_to :interpreter
  belongs_to :disc
end