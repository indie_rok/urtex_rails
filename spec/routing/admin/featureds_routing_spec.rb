require "rails_helper"

RSpec.describe Admin::FeaturedsController, type: :routing do
  describe "routing" do

    it "routes to #index" do
      expect(:get => "/admin/featureds").to route_to("admin/featureds#index")
    end

    it "routes to #new" do
      expect(:get => "/admin/featureds/new").to route_to("admin/featureds#new")
    end

    it "routes to #show" do
      expect(:get => "/admin/featureds/1").to route_to("admin/featureds#show", :id => "1")
    end

    it "routes to #edit" do
      expect(:get => "/admin/featureds/1/edit").to route_to("admin/featureds#edit", :id => "1")
    end

    it "routes to #create" do
      expect(:post => "/admin/featureds").to route_to("admin/featureds#create")
    end

    it "routes to #update via PUT" do
      expect(:put => "/admin/featureds/1").to route_to("admin/featureds#update", :id => "1")
    end

    it "routes to #update via PATCH" do
      expect(:patch => "/admin/featureds/1").to route_to("admin/featureds#update", :id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/admin/featureds/1").to route_to("admin/featureds#destroy", :id => "1")
    end

  end
end
