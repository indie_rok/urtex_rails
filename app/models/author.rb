class Author < ActiveRecord::Base

  validates :name, presence: true, uniqueness: true

  has_one :attachment, as: :attachmentable

  has_many :shelves, dependent: :destroy
  has_many :discs, through: :shelves
end
