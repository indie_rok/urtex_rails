require 'rails_helper'

RSpec.describe Interpreter, type: :model do
  it{ should have_many :performances }
  it{ should have_many(:tracks).through(:performances) }
end
