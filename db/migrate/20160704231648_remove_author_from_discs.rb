class RemoveAuthorFromDiscs < ActiveRecord::Migration
  def change
    remove_column :discs, :author_id
  end
end
