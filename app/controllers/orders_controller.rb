class OrdersController < ApplicationController

  before_action :require_login
  before_action :set_order, only: [:show]
  before_action :redirectIfUserDoesNotOwnsThisOrder

  def show
    @items = []
    @order.details.each do |item|
      if item.detailmentable_type == "Disc"
        item = Disc.find(item.detailmentable_id)
      elsif item.detailmentable_type = "Track"
        item = Track.find(item.detailmentable_id)
      end
    @items.push item
    end
    @payment = @order.payment
  end

  def download
    send_file params[:url], :type=>"audio/mp3", :filename => "filenamehere.mp3"
  end

  private
  def set_order
    @order = Order.find(params[:id])
  end

  def redirectIfUserDoesNotOwnsThisOrder
     if current_user.id != @order.user_id
       flash[:danger] = t("flash_no_tu_orden")
       redirect_to root_path
     end
  end

end
