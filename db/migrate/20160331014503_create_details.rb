class CreateDetails < ActiveRecord::Migration
  def change
    create_table :details do |t|
      t.references :detailmentable, polymorphic: true
      t.belongs_to :order
      t.timestamps null: false
    end
  end
end
