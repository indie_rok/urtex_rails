require 'rails_helper'

RSpec.describe "admin/featureds/index", type: :view do
  before(:each) do
    assign(:admin_featureds, [
      Admin::Featured.create!(
        :url => "Url",
        :title => "Title"
      ),
      Admin::Featured.create!(
        :url => "Url",
        :title => "Title"
      )
    ])
  end

  it "renders a list of admin/featureds" do
    render
    assert_select "tr>td", :text => "Url".to_s, :count => 2
    assert_select "tr>td", :text => "Title".to_s, :count => 2
  end
end
