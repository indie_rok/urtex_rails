class ChangeNameInGenres < ActiveRecord::Migration
  def change
    rename_column :genres, :name, :name_es
  end
end
