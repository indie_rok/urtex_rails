class AddSampleUrlToTracks < ActiveRecord::Migration
  def change
    add_column :tracks, :sampleUrl, :string
  end
end
