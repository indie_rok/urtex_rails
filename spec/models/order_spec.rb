require 'rails_helper'

RSpec.describe Order, type: :model do
  it{should belong_to :user}
  it{should have_many :details}
  it { should have_many(:discs).through(:details) }
  it { should have_many(:tracks).through(:details) }
  it{ should have_one :payment  }
end
