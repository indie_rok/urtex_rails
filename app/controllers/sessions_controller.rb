class SessionsController < ApplicationController

  before_action :confirm_login, only: [:new]

  def new
  end

  def create
    user = User.find_by_email(params[:email])

    if user && user.authenticate(params[:password])
      session[:user_id] = user.id
      flash[:success] = t('flash_login_exito')
      if user.admin?
        redirect_to admin_panel_path
      else
        redirect_to root_path
      end
    else
      flash[:danger] = t('flash_no_usuario')
      redirect_to new_user_path
    end
  end

  def destroy
    if session[:user_id]
      reset_session
      flash[:success] = t('flash_sesion_cerrada')
      redirect_to root_path
    else
      redirect_to root_path
    end
  end

  private
  def session_params
    params.permit(:email,:password)
  end

end
