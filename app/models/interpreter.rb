class Interpreter < ActiveRecord::Base
  translates :name

  validates :name, :presence => true

  has_many :performances, dependent: :destroy
  has_many :tracks, through: :performances
  has_many :discs, through: :performances
end