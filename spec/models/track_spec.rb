require 'rails_helper'

RSpec.describe Track, type: :model do
  it{ should belong_to :disc }
  it{ should have_many :performances }
  it{ should have_many(:interpreters).through(:performances) }
end
