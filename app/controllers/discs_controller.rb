class DiscsController < ApplicationController
  before_action :set_disc, only: [:show]

  def show
    @tracks = @disc.tracks.order(:title)
  end

  def search
    @discs = Disc.search_by_title params[:disc]
  end

  private
  def set_disc
    @disc = Disc.find(params[:id])
  end
end
