require 'rails_helper'

RSpec.describe "admin/interpreters/edit", type: :view do
  before(:each) do
    @admin_interpreter = assign(:admin_interpreter, Admin::Interpreter.create!(
      :name => "MyString"
    ))
  end

  it "renders the edit admin_interpreter form" do
    render

    assert_select "form[action=?][method=?]", admin_interpreter_path(@admin_interpreter), "post" do

      assert_select "input#admin_interpreter_name[name=?]", "admin_interpreter[name]"
    end
  end
end
