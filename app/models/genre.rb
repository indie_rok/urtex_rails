class Genre < ActiveRecord::Base

  translates :name

  validates :name, presence: true

  has_many :discs
  has_one :attachment, as: :attachmentable
end
