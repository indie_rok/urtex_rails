# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20160725173417) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "admin_featureds", force: :cascade do |t|
    t.string   "url"
    t.string   "title_es"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string   "title_en"
  end

  create_table "attachments", force: :cascade do |t|
    t.string   "url",                 null: false
    t.string   "amazonId",            null: false
    t.integer  "attachmentable_id"
    t.string   "attachmentable_type"
    t.datetime "created_at",          null: false
    t.datetime "updated_at",          null: false
  end

  create_table "authors", force: :cascade do |t|
    t.string   "name",       null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "details", force: :cascade do |t|
    t.integer  "detailmentable_id"
    t.string   "detailmentable_type"
    t.integer  "order_id"
    t.datetime "created_at",          null: false
    t.datetime "updated_at",          null: false
  end

  create_table "discs", force: :cascade do |t|
    t.string   "title_es",                       null: false
    t.text     "description_es"
    t.decimal  "price",          default: 120.0
    t.integer  "genre_id"
    t.datetime "created_at",                     null: false
    t.datetime "updated_at",                     null: false
    t.string   "discUrl"
    t.string   "title_en"
    t.string   "description_en"
  end

  create_table "genres", force: :cascade do |t|
    t.string   "name_es",                    null: false
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
    t.string   "name_en"
    t.boolean  "collection", default: false
  end

  create_table "interpreters", force: :cascade do |t|
    t.string   "name_es",    null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string   "name_en"
  end

  create_table "orders", force: :cascade do |t|
    t.integer  "user_id"
    t.decimal  "totalPrice"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "payments", force: :cascade do |t|
    t.integer  "order_id"
    t.integer  "user_id"
    t.string   "amount",     null: false
    t.string   "status",     null: false
    t.string   "code",       null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "payments", ["user_id"], name: "index_payments_on_user_id", using: :btree

  create_table "performances", force: :cascade do |t|
    t.integer "track_id"
    t.integer "interpreter_id"
    t.integer "disc_id"
  end

  create_table "shelves", force: :cascade do |t|
    t.integer "author_id"
    t.integer "disc_id"
  end

  create_table "tracks", force: :cascade do |t|
    t.string   "url",                       null: false
    t.string   "title",                     null: false
    t.string   "amazonId",                  null: false
    t.string   "flocUrl"
    t.integer  "disc_id",                   null: false
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
    t.string   "sampleUrl"
    t.decimal  "price",      default: 12.0
  end

  create_table "users", force: :cascade do |t|
    t.string   "email",                           null: false
    t.string   "password_digest",                 null: false
    t.boolean  "admin",           default: false
    t.datetime "created_at",                      null: false
    t.datetime "updated_at",                      null: false
  end

  add_foreign_key "payments", "users"
end
