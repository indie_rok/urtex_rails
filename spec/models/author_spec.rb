require 'rails_helper'

RSpec.describe Author, type: :model do
  it{ should have_one(:attachment) }
  it{ should have_many :discs }
end
