class CreatePerformances < ActiveRecord::Migration
  def change
    create_table :performances do |t|
      t.belongs_to :track
      t.belongs_to :interpreter
    end
  end
end
