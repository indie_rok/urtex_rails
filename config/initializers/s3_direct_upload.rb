S3DirectUpload.config do |c|
  c.access_key_id = ENV["s3_access_key"]       # your access key id
  c.secret_access_key = ENV["s3_key_id"]   # your secret access key
  c.bucket = ENV["s3_bucket"]              # your bucket name
  c.region = ENV["s3_region"]             # region prefix of your bucket url. This is _required_ for the non-default AWS region, eg. "s3-eu-west-1"
  c.url = ENV["s3_url"]                # S3 API endpoint (optional), eg. "https://#{c.bucket}.s3.amazonaws.com/"
end
