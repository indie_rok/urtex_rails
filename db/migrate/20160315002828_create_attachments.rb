class CreateAttachments < ActiveRecord::Migration
  def change
    create_table :attachments do |t|
      t.string :url, null: false
      t.string :amazonId, null: false, uniqueness: true
      t.references :attachmentable, polymorphic: true
      t.timestamps null: false
    end
  end
end
