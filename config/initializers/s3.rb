Aws.config.update({
  region: ENV["s3_region"],
  credentials: Aws::Credentials.new(ENV["s3_access_key"], ENV["s3_key_id"] )
})
