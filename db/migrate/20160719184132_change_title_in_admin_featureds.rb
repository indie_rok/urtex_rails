class ChangeTitleInAdminFeatureds < ActiveRecord::Migration
  def change
    rename_column :admin_featureds, :title, :title_es
  end
end
