class ProfilesController < ApplicationController
  before_action :require_login

  def show
    @user = current_user
    @compras = @user.payments
  end

end
